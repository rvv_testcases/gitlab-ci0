FROM gcc:7 AS gcc-builder
RUN apt-get update && \
  apt-get install -y --no-install-recommends build-essential autoconf automake libtool groff perl wget tar gzip && \
  rm -rf /var/lib/apt/lists/*
RUN cd /opt && wget http://luajit.org/download/LuaJIT-2.1.0-beta3.tar.gz \
  https://github.com/simplresty/ngx_devel_kit/archive/v0.3.1rc1.tar.gz \
  https://github.com/openresty/lua-nginx-module/archive/v0.10.13rc1.tar.gz \
  http://nginx.org/download/nginx-1.15.3.tar.gz && tar xzf LuaJIT-2.1.0-beta3.tar.gz && \
  tar xzf v0.3.1rc1.tar.gz && tar xzf v0.10.13rc1.tar.gz && tar xzf nginx-1.15.3.tar.gz && ls -alth
RUN cd /opt/LuaJIT-2.1.0-beta3 && make && make install
ENV LUAJIT_LIB=/usr/local/lib/libluajit-5.1.so.2.1.0
ENV LUAJIT_INC=/usr/local/include/luajit-2.1
RUN cd /opt/nginx-1.15.3 && ./configure --prefix=/opt/nginx \
         --with-ld-opt="-Wl,-rpath,/opt/LuaJIT-2.1.0-beta3/lib" \
         --add-module=/opt/ngx_devel_kit-0.3.1rc1 \
         --add-module=/opt/lua-nginx-module-0.10.13rc1 && \
  make -j2 && make install

FROM debian:stretch-slim
WORKDIR /opt
COPY --from=gcc-builder /opt/nginx /opt/nginx
COPY --from=gcc-builder /usr/local/lib/libluajit-5.1.so.2.1.0 /usr/local/lib/libluajit-5.1.so.2.1.0
COPY --from=gcc-builder /usr/local/include/luajit-2.1 /usr/local/include/luajit-2.1
RUN ln -sf libluajit-5.1.so.2.1.0 /usr/local/lib/libluajit-5.1.so && \
  ln -sf libluajit-5.1.so.2.1.0 /usr/local/lib/libluajit-5.1.so.2
COPY nginx.conf /opt/nginx/nginx.conf
COPY index.html /opt/nginx/html/index.html

EXPOSE 80
STOPSIGNAL SIGTERM
ENV LD_LIBRARY_PATH=/usr/local/lib:$LD_LIBRARY_PATH
CMD ["/opt/nginx/sbin/nginx", "-c", "/opt/nginx/nginx.conf", "-g", "daemon off;"]
